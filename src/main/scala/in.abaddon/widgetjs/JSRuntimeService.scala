package in.abaddon.widgetjs

import android.app.IntentService
import android.appwidget.AppWidgetManager
import android.content.{ComponentName, Context, Intent}
import com.eclipsesource.v8.V8

class JSRuntimeService extends IntentService("JSRuntimeService"){
    override def onHandleIntent(intent: Intent): Unit = {
        Option(intent).foreach(i => i.getAction match {
            case JSRuntimeService.ACTION_RUN =>
                val sharedPref = getSharedPreferences("sp", Context.MODE_PRIVATE)
                val source = sharedPref.getString(JSRuntimeService.EXTRA_SOURCE, "")

                val runtime = V8.createV8Runtime()
                val result = runtime.executeStringScript(source)
                runtime.release()

                updateWidget(result)
        })
    }

    private def updateWidget(result: String) = {
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val widgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(this, classOf[SimpleWidgetProvider]))
        widgetIds.foreach(i => SimpleWidgetProvider.updateAppWidget(this, appWidgetManager, widgetIds, result))
    }
}

object JSRuntimeService{
    val ACTION_RUN = "in.abaddon.widgetjs.RUN"
    val EXTRA_SOURCE = "SOURCE"
    val EXTRA_RESULT = "RESULT"
}
