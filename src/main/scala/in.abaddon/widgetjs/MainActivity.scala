package in.abaddon.widgetjs

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.{Button, EditText, Toast}
import androidx.appcompat.app.AppCompatActivity

class MainActivity extends AppCompatActivity {
    private lazy val button: Button = findViewById(R.id.button)
    private lazy val edittext: EditText = findViewById(R.id.edittext)

    override protected def onCreate(savedInstanceState: Bundle) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener(new View.OnClickListener {
            override def onClick(v: View): Unit = {
                val sharedPref = v.getContext.getSharedPreferences("sp", Context.MODE_PRIVATE).edit()
                val source = edittext.getText.toString

                sharedPref.putString(JSRuntimeService.EXTRA_SOURCE, source)
                sharedPref.commit()
                Toast.makeText(v.getContext, "saved", Toast.LENGTH_SHORT).show()
            }
        })
    }

}
