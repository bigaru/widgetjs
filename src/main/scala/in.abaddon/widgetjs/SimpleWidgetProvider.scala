package in.abaddon.widgetjs

import android.app.PendingIntent
import android.appwidget.{AppWidgetManager, AppWidgetProvider}
import android.content.{Context, Intent}
import android.widget.RemoteViews

class SimpleWidgetProvider extends AppWidgetProvider {
    override def onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: Array[Int]): Unit = {
        SimpleWidgetProvider.updateAppWidget(context, appWidgetManager, appWidgetIds)
    }
}
object SimpleWidgetProvider{
    def updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: Array[Int], result: String = ""): Unit = {

        appWidgetIds.foreach { id =>
            val remoteViews = new RemoteViews(context.getPackageName, R.layout.widget_foo)
            remoteViews.setTextViewText(R.id.w_textview, result)

            val intent = new Intent(context, classOf[JSRuntimeService])
            intent.setAction(JSRuntimeService.ACTION_RUN)

            val pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            remoteViews.setOnClickPendingIntent(R.id.w_button, pendingIntent)

            appWidgetManager.updateAppWidget(id, remoteViews)
        }
    }
}
